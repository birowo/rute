package nethttp

import (
	"log"
	"net/http"

	"gitlab.com/birowo/rute"
)

type Method struct {
	GET, POST, PUT, PATCH, DELETE http.HandlerFunc
}
type Mux struct {
	rute.Tr[Method]
}
type RW struct {
	http.ResponseWriter
	rute.UserValue
}

func (m *Mux) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	defer func() {
		if err := recover(); err != nil {
			log.Println(err)
		}
	}()
	v, prms_, uv := m.Lookup([]byte(r.RequestURI))
	switch {
	case uv.Params != nil:
		defer m.Put(prms_)
		fallthrough
	case uv.Wildcard != nil:
		w = RW{w, uv}
	}
	switch r.Method {
	case "GET":
		if v.GET == nil {
			http.Error(w, "", http.StatusMethodNotAllowed)
			return
		}
		v.GET(w, r)
	case "POST":
		if v.POST == nil {
			http.Error(w, "", http.StatusMethodNotAllowed)
			return
		}
		v.POST(w, r)
	case "PUT":
		if v.PUT == nil {
			http.Error(w, "", http.StatusMethodNotAllowed)
			return
		}
		v.PUT(w, r)
	case "PATCH":
		if v.PATCH == nil {
			http.Error(w, "", http.StatusMethodNotAllowed)
			return
		}
		v.PATCH(w, r)
	case "DELETE":
		if v.DELETE == nil {
			http.Error(w, "", http.StatusMethodNotAllowed)
			return
		}
		v.DELETE(w, r)
	default:
		http.Error(w, "", http.StatusNotFound)
	}
}

type Mux_ struct {
	get, post, put, patch, delete rute.Tr[http.HandlerFunc]
}

func (m *Mux_) Get(path string, h http.HandlerFunc) {
	m.get.Handle(path, h)
}
func (m *Mux_) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	defer func() {
		if err := recover(); err != nil {
			log.Println(err)
		}
	}()
	switch r.Method {
	case "GET":
		h, prms_, uv := m.get.Lookup([]byte(r.RequestURI))
		if h == nil {
			http.Error(w, "", http.StatusNotFound)
		}
		switch {
		case uv.Params != nil:
			defer m.get.Put(prms_)
			fallthrough
		case uv.Wildcard != nil:
			w = RW{w, uv}
		}
	case "POST":
		h, prms_, uv := m.post.Lookup([]byte(r.RequestURI))
		if h == nil {
			http.Error(w, "", http.StatusNotFound)
		}
		switch {
		case uv.Params != nil:
			defer m.post.Put(prms_)
			fallthrough
		case uv.Wildcard != nil:
			w = RW{w, uv}
		}
	case "PUT":
		h, prms_, uv := m.put.Lookup([]byte(r.RequestURI))
		if h == nil {
			http.Error(w, "", http.StatusNotFound)
		}
		switch {
		case uv.Params != nil:
			defer m.put.Put(prms_)
			fallthrough
		case uv.Wildcard != nil:
			w = RW{w, uv}
		}
	case "PATCH":
		h, prms_, uv := m.patch.Lookup([]byte(r.RequestURI))
		if h == nil {
			http.Error(w, "", http.StatusNotFound)
		}
		switch {
		case uv.Params != nil:
			defer m.patch.Put(prms_)
			fallthrough
		case uv.Wildcard != nil:
			w = RW{w, uv}
		}
	case "DELETE":
		h, prms_, uv := m.delete.Lookup([]byte(r.RequestURI))
		if h == nil {
			http.Error(w, "", http.StatusNotFound)
		}
		switch {
		case uv.Params != nil:
			defer m.delete.Put(prms_)
			fallthrough
		case uv.Wildcard != nil:
			w = RW{w, uv}
		}
	default:
		http.Error(w, "", http.StatusMethodNotAllowed)
	}
}
