package main

import (
	"context"
	"fmt"
	"io"
	"math/rand"
	"net"
	"net/http"
	"net/http/httptest"

	"github.com/valyala/fasthttp"
	"github.com/valyala/fasthttp/fasthttputil"
	"gitlab.com/birowo/rute"
	httpfast "gitlab.com/birowo/rute/fasthttp"
	"gitlab.com/birowo/rute/nethttp"
)

var (
	str    = []byte("ABCDEFGHI")
	strLen = len(str)
	swap   = func(i, j int) {
		str[i], str[j] = str[j], str[i]
	}
)

func s0() {
	rh := func(ctx *fasthttp.RequestCtx) {
		fmt.Fprintf(
			ctx,
			"\nmethod: %s\npath: %s\nuservalue: %q\n",
			ctx.Method(),
			ctx.Path(),
			ctx.UserValueBytes(httpfast.Uservalue),
		)
	}
	m := &httpfast.Mux{}
	m.Handle("/abc/:/:/ghi/*", httpfast.Method{GET: rh})
	m.Handle("/abc/:/def/:/*", httpfast.Method{GET: rh})
	s := &fasthttp.Server{Handler: m.Handler}
	ln := fasthttputil.NewInmemoryListener()
	defer ln.Close()
	go s.Serve(ln)
	client := http.Client{Transport: &http.Transport{
		DialContext: func(_ context.Context, _, _ string) (net.Conn, error) {
			return ln.Dial()
		},
	}}
	for i := 0; i < 3; i++ {
		rand.Shuffle(strLen, swap)
		prm1 := rute.StrSlc(str[:3])
		prm2 := rute.StrSlc(str[3:6])
		wc := rute.StrSlc(str[6:])
		println("param1:", prm1, ",param2:", prm2, ",wildcard:", wc)
		resp, _ := client.Get("http://-/abc/" + prm1 + "/" + prm2 + "/ghi/" + wc)
		body, _ := io.ReadAll(resp.Body)
		print("body:", rute.StrSlc(body))
		resp, _ = client.Get("http://-/abc/" + prm1 + "/def/" + prm2 + "/" + wc)
		body, _ = io.ReadAll(resp.Body)
		println("body:", rute.StrSlc(body))
	}
}
func s1() {
	rh := func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(
			w,
			"\nmethod: %s\npath: %s\nuservalue: %q\n",
			r.Method,
			r.RequestURI,
			w.(nethttp.RW).UserValue,
		)
	}
	m := &nethttp.Mux{}
	m.Handle("/abc/:/:/ghi/*", nethttp.Method{GET: rh})
	m.Handle("/abc/:/def/:/*", nethttp.Method{GET: rh})
	for i := 0; i < 3; i++ {
		rand.Shuffle(strLen, swap)
		prm1 := rute.StrSlc(str[:3])
		prm2 := rute.StrSlc(str[3:6])
		wc := rute.StrSlc(str[6:])
		println("param1:", prm1, ",param2:", prm2, ",wildcard:", wc)
		w := httptest.NewRecorder()
		m.ServeHTTP(w, httptest.NewRequest(http.MethodGet, "/abc/"+prm1+"/"+prm2+"/ghi/"+wc, nil))
		resp := w.Result()
		body, _ := io.ReadAll(resp.Body)
		print("body:", rute.StrSlc(body))
		w = httptest.NewRecorder()
		m.ServeHTTP(w, httptest.NewRequest(http.MethodGet, "/abc/"+prm1+"/def/"+prm2+"/"+wc, nil))
		resp = w.Result()
		body, _ = io.ReadAll(resp.Body)
		println("body:", rute.StrSlc(body))
	}
}
func main() {
	println("fasthttp:")
	s0()
	println("net/http:")
	s1()
}
