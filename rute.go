package rute

import (
	"sync"
	"unsafe"

	"github.com/dolthub/swiss"
)

type KV[V any] struct {
	*swiss.Map[string, *KV[V]]
	v V
}
type Tr[V any] struct {
	KV[V]
	prmsN int
	sync.Pool
}
type UserValue struct {
	Params   [][]byte
	Wildcard []byte
}

const sz = 32

func (tr *Tr[V]) Handle(k string, v V) {
	if tr.Map == nil || tr.New == nil {
		tr.Map = swiss.NewMap[string, *KV[V]](sz)
		tr.New = func() any {
			//println("params alloc:", tr.prmsN)
			return make([][]byte, 0, tr.prmsN)
		}
	}
	var (
		v_    V
		kv_   *KV[V]
		ok    bool
		prmsN int
	)
	kv := &tr.KV
	i := 0
	for j, c := range k {
		if c == '/' {
			if k[i] == ':' {
				prmsN++
				if tr.prmsN < prmsN {
					tr.prmsN = prmsN
				}
			}
			if kv_, ok = kv.Get(k[i:j]); !ok {
				kv_ = &KV[V]{swiss.NewMap[string, *KV[V]](sz), v_}
				kv.Put(k[i:j], kv_)
			}
			kv = kv_
			i = j + 1
		}
	}
	kv.Put(k[i:], &KV[V]{nil, v})
	if k[i] == ':' {
		prmsN++
		if tr.prmsN < prmsN {
			tr.prmsN = prmsN
		}
	}
}
func StrSlc(slc []byte) string {
	return *(*string)(unsafe.Pointer(&slc))
}
func (tr *Tr[V]) Lookup(k []byte) (v V, prms_ any, uv UserValue) {
	//uv.Params: params, wc: wildcard
	var (
		kv_ *KV[V]
		ok  bool
	)
	kLen := len(k)
	kv := &tr.KV
	i, j := 0, 0
	for {
		for j < kLen && k[j] != '/' {
			j++
		}
		if j == kLen {
			if kv_, ok = kv.Get(StrSlc(k[i:])); !ok {
				if kv_, ok = kv.Get(":"); !ok {
					if kv_, ok = kv.Get("*"); !ok {
						//println("not found")
						return
					}
					uv.Wildcard = k[i:]
					v = kv_.v
					return
				}
				if uv.Params == nil {
					prms_ = tr.Get()
					uv.Params = prms_.([][]byte)
				}
				uv.Params = append(uv.Params, k[i:])
			}
			v = kv_.v
			return
		}
		if kv_, ok = kv.Get(StrSlc(k[i:j])); !ok {
			if kv_, ok = kv.Get(":"); !ok {
				if kv_, ok = kv.Get("*"); !ok {
					//println("not found")
					return
				}
				uv.Wildcard = k[i:]
				v = kv_.v
				return
			}
			if uv.Params == nil {
				prms_ = tr.Get()
				uv.Params = prms_.([][]byte)
			}
			uv.Params = append(uv.Params, k[i:j])
		}
		kv = kv_
		j++
		i = j
	}
}
