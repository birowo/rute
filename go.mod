module gitlab.com/birowo/rute

go 1.22

require (
	github.com/dolthub/swiss v0.2.1
	github.com/valyala/fasthttp v1.54.0
)

require (
	github.com/andybalholm/brotli v1.1.0 // indirect
	github.com/dolthub/maphash v0.1.0 // indirect
	github.com/klauspost/compress v1.17.7 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
)
