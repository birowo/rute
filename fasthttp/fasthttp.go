package fasthttp

import (
	"log"

	"github.com/valyala/fasthttp"
	"gitlab.com/birowo/rute"
)

type Method struct {
	GET, POST, PUT, PATCH, DELETE fasthttp.RequestHandler
}
type Mux struct {
	rute.Tr[Method]
}

var Uservalue = []byte("")

func (m *Mux) Handler(ctx *fasthttp.RequestCtx) {
	defer func() {
		if err := recover(); err != nil {
			log.Println(err)
		}
	}()
	v, prms_, uv := m.Lookup(ctx.Path())
	switch {
	case uv.Params != nil:
		defer m.Put(prms_)
		fallthrough
	case uv.Wildcard != nil:
		ctx.SetUserValueBytes(Uservalue, uv)
	}
	switch rute.StrSlc(ctx.Method()) {
	case "GET":
		if v.GET == nil {
			ctx.Error("", fasthttp.StatusMethodNotAllowed)
			return
		}
		v.GET(ctx)
	case "POST":
		if v.POST == nil {
			ctx.Error("", fasthttp.StatusMethodNotAllowed)
			return
		}
		v.POST(ctx)
	case "PUT":
		if v.PUT == nil {
			ctx.Error("", fasthttp.StatusMethodNotAllowed)
			return
		}
		v.PUT(ctx)
	case "PATCH":
		if v.PATCH == nil {
			ctx.Error("", fasthttp.StatusMethodNotAllowed)
			return
		}
		v.PATCH(ctx)
	case "DELETE":
		if v.DELETE == nil {
			ctx.Error("", fasthttp.StatusMethodNotAllowed)
			return
		}
		v.DELETE(ctx)
	default:
		ctx.Error("", fasthttp.StatusNotFound)
	}
}
